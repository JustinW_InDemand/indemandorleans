using System;
using System.Threading.Tasks;
using Orleans;
using System.Collections.Generic;
using System.Linq;

namespace InDemand.GrainInterfaces
{
    public interface IConferenceGrain : IGrainWithGuidKey
    {
        Task<ConferenceState> JoinParticipantToConference(string participantId);
        Task InviteParticipantToConference(string participantId);
        Task<ConferenceState> DeclineParticipantFromConference(string participantId);
        Task<ConferenceState> LeaveParticipantFromConference(string participantId);
        Task<ConferenceState> GetConferenceState();
        Task<ConferenceSummary> GetConferenceSummary();
        Task RouteConference(CreateConferenceRequest createConferenceRequest);
    }

    [Flags]
    public enum ConferenceState : short
    {
        // First byte: MAIN states
        Unknown = 0,
        AwaitingParticipants = 1,
        InProgress = 2,
        Finished = 4,
        // 8
        // 16
        // 32
        // 64
        // 128

        // Second byte: SUB states (Normal is all bits cleared)
        BusySignal = 256,
        // 512
        // 1024
        // 2048
        // 4096
        // 8192
        // 16384
        // 32768
    }

    public class CreateConferenceRequest
    {
        public string Language { get; set; }
        public string Gender { get; set; }
        public string Skill { get; set; }
    }

    public enum ConferenceParticipantState
    {
        Unknown,
        Invited,
        Disinvited,
        Declined,
        Joined,
        Trouble,
        Left     
    }

    public class ConferenceParticipantSummary
    {
        public string ParticipantId { get; set; }
        public ConferenceParticipantState ConferenceParticipantState { get; set; }
    }

    public static class ConferenceParticipantSummaryExtensions
    {
        public static ConferenceParticipantSummary Find(this IEnumerable<ConferenceParticipantSummary> collection, string participantId)
        {
            return collection.Where((x) => { return x.ParticipantId == participantId; }).FirstOrDefault();
        }

        public static IEnumerable<ConferenceParticipantSummary> FindByState(this IEnumerable<ConferenceParticipantSummary> collection, ConferenceParticipantState conferenceParticipantState)
        {
            return collection.Where((x) => { return x.ConferenceParticipantState == conferenceParticipantState; });
        }
    }

    public class ConferenceSummary
    {
        public Guid ConferenceId { get; set; }
        public ConferenceState ConferenceState { get; set; }
        public ConferenceParticipantSummary[] ConferenceParticipantSummaries { get; set; }
    }
}
