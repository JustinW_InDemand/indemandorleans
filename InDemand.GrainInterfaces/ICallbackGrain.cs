﻿using Orleans;
using System;
using System.Threading.Tasks;

namespace InDemand.GrainInterfaces
{
    public interface ICallback : IGrainObserver
    {
        void InviteParticipantToConference(string participantId, Guid conferenceId);
    }

    public interface ICallbackGrain : IGrainWithIntegerKey
    {
        Task SubscribeObserver(ICallback observer);
        Task<bool> CallbackInviteParticipantToConference(string participantId, Guid conferenceId);
    }
}
