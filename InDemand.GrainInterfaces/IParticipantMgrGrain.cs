using Orleans;
using System;
using System.Threading.Tasks;

namespace InDemand.GrainInterfaces
{
    public interface IParticipantMgrGrain : IGrainWithIntegerKey
    {
        Task AddParticipant(string participantId);
        Task RemoveParticipant(string participantId);
        Task<ParticipantSummary[]> GetParticipantSummaries();
    }
}
