using System;
using System.Threading.Tasks;
using Orleans;

namespace InDemand.GrainInterfaces
{
    public interface IParticipantGrain : IGrainWithStringKey
    {
        Task<Guid> CreateConferenceForParticipant(CreateConferenceRequest createConferenceRequest);
        Task<ConferenceState> JoinParticipantToConference(Guid conferenceId);
        Task<bool> InviteParticipantToConference(Guid conferenceId);
        Task<ConferenceState> LeaveParticipantFromConference();
        Task<ParticipantState> GetParticipantState();
        Task ResetParticipantState(bool hard);
        Task<bool> SetParticipantState(ParticipantState participantState, Guid conferenceId);
        Task<bool> LoginParticipant(string password);
        Task<ParticipantSummary> GetParticipantSummary();
    }

    public enum ParticipantState
    {
        Unknown,
        Offline,
        Online,
        OnBreak,
        Invited,
        InConference
    }

    public class ParticipantSummary
    {
        public string ParticipantId { get; set; }
        public ParticipantState ParticipantState { get; set; }
        public Guid ConferenceId { get; set; }
    }
}
