﻿var indemand = {
    webSocket: null,
    participantId: null,
    invitedConferenceId: null,
    inviteTimer: null,
    inConference: false,
    pendingJoin: false,

    verb: {
        post: function (url, payload, cb) {
            //console.log('post:', url, payload);
            $.post(url, payload, cb);
        },

        get: function (url, cb) {
            //console.log('get:', url);
            $.get(url, cb);
        }
    },

    ajax: {
        createConference: function (cb) {
            indemand.verb.post('/Conference/Create/' + indemand.participantId, { jsonData: JSON.stringify({ Language: 'English', Gender: 'Any', Skill: 'Mad Skillz' }) }, cb);
        },

        joinConference: function (conferenceId, cb) {
            indemand.verb.post('/Conference/Join/' + indemand.participantId + '/' + conferenceId, {}, cb);
        },

        declineConference: function (conferenceId, cb) {
            indemand.verb.post('/Conference/Decline/' + indemand.participantId + '/' + conferenceId, {}, cb);
        },

        leaveConference: function (cb) {
            indemand.verb.get('/Conference/Leave/' + indemand.participantId, cb);
        },

        listConferences: function (cb) {
            indemand.verb.get('/Domain/ListConferences', cb);
        },

        loginParticipant: function (cb) {
            indemand.verb.post('/Participant/Login/' + indemand.participantId, {}, cb);
        },

        breakParticipant: function (cb) {
            indemand.verb.get('/Participant/Break/' + indemand.participantId, cb);
        },

        resumeParticipant: function (cb) {
            indemand.verb.get('/Participant/Resume/' + indemand.participantId, cb);
        },

        logoutParticipant: function (cb) {
            indemand.verb.get('/Participant/Logout/' + indemand.participantId, cb);
        },

        listParticipants: function (cb) {
            indemand.verb.get('/Domain/ListParticipants', cb);
        }
    },

    controllers: {
        refreshConferenceList: function () {
            indemand.ajax.listConferences(indemand.ui.renderConferenceList);
        },

        refreshParticipantList: function () {
            indemand.ajax.listParticipants(indemand.ui.renderParticipantList);
        },

        refreshAll: function () {
            indemand.controllers.refreshParticipantList();
            indemand.controllers.refreshConferenceList();
        },

        createConference: function () {
            indemand.ui.logEvent('action create');
            indemand.inConference = true;
            indemand.pendingJoin = false;
            indemand.ui.turnOffInvite(null);
            indemand.ajax.createConference(indemand.controllers.refreshAll);
        },

        joinConference: function (conferenceId) {
            if (!conferenceId) return;
            indemand.ui.logEvent('action join: ' + conferenceId);
            indemand.inConference = true;
            indemand.pendingJoin = false;
            indemand.ui.turnOffInvite(conferenceId);
            indemand.ajax.joinConference(conferenceId, indemand.controllers.refreshAll);
        },

        declineConference: function (conferenceId, cb) {
            if (!conferenceId) return;
            indemand.ui.logEvent('action decline: ' + conferenceId);
            indemand.ui.turnOffInvite(conferenceId);
            indemand.ajax.declineConference(conferenceId, cb || indemand.controllers.refreshAll); 
            indemand.inConference = false;
            indemand.pendingJoin = false;
        },

        leaveConference: function () {
            indemand.ui.logEvent('action leave');
            indemand.ajax.leaveConference(indemand.controllers.refreshAll);
            indemand.inConference = false;
            indemand.pendingJoin = false;
        },

        loginParticipant: function () {
            indemand.participantId = $("#loginInput").val().trim();
            if (!indemand.participantId) return;
            indemand.ui.logEvent('action login: ' + indemand.participantId);
            indemand.ajax.loginParticipant(
                function () {
                    $("#identityLabel").html(indemand.participantId);
                    indemand.controllers.refreshAll();
                }
            );

            indemand.inConference = false;
            indemand.pendingJoin = false;
        },

        breakParticipant: function () {
            indemand.ui.logEvent('action break');
            if (indemand.invitedConferenceId) {
                indemand.controllers.declineConference(indemand.invitedConferenceId,
                    indemand.ajax.breakParticipant.bind(this, indemand.controllers.refreshAll)
                );
            } else {
                indemand.ajax.breakParticipant(indemand.controllers.refreshAll);
            }

            indemand.inConference = false;
            indemand.pendingJoin = false;
        },

        resumeParticipant: function () {
            indemand.ui.logEvent('action resume');
            indemand.ajax.resumeParticipant(indemand.controllers.refreshAll);
            indemand.inConference = false;
            indemand.pendingJoin = false;
        },

        logoutParticipant: function () {
            indemand.ui.logEvent('action logout');

            if (indemand.invitedConferenceId) {
                indemand.controllers.declineConference(indemand.invitedConferenceId,
                    indemand.ajax.logoutParticipant.bind(this, indemand.controllers.refreshAll)
                );
            } else {
                indemand.ajax.logoutParticipant(indemand.controllers.refreshAll);
            }

            indemand.inConference = false;
            indemand.pendingJoin = false;
        }
    },

    ui: {
        turnOffInvite: function (conferenceId) {
            if (indemand.inviteTimer && (indemand.invitedConferenceId === conferenceId || conferenceId === null)) {
                clearTimeout(indemand.inviteTimer);
                indemand.inviteTimer = null;
            }

            indemand.invitedConferenceId = null;
        },

        renderConferenceList: function (data) {
            //console.log('conference list:', data);
            var rawTemplate = $("#conferencesTemplate").html();
            var compiledTemplate = Handlebars.compile(rawTemplate);
            var renderedTemplate = compiledTemplate(data);
            $("#conferencesPlaceholder").html(renderedTemplate);
        },

        renderParticipantList: function (data) {
            //console.log('participant list:', data);
            var rawTemplate = $("#participantsTemplate").html();
            var compiledTemplate = Handlebars.compile(rawTemplate);
            var renderedTemplate = compiledTemplate(data);
            $("#participantsPlaceholder").html(renderedTemplate);
        },

        logEvent: function (event) {
            var time = new Date().toISOString();
            $("#eventsArea").append(time + ' ' + event + '\n');
        }
    }
};

function reconnectWebSocket() {
    setSocketState();
    indemand.ui.logEvent('Reconnecting in 5 seconds.');
    setTimeout(
        connectWebSocket,
        5000
    );
}

function connectWebSocket() {
    var scheme = document.location.protocol === "https:" ? "wss" : "ws";
    var port = document.location.port ? ":" + document.location.port : "";
    var connectionUrl = scheme + "://" + document.location.hostname + port + "/ws";

    indemand.webSocket = new WebSocket(connectionUrl);
    indemand.webSocket.onopen = setSocketState;
    indemand.webSocket.onclose = reconnectWebSocket;
    indemand.webSocket.onerror = reconnectWebSocket;
    indemand.webSocket.onmessage = function (event) {
        if (!indemand.inConference && !indemand.invitedConferenceId) {
            indemand.invitedConferenceId = event.data;

            if ($("#autoJoin")[0].checked) {
                var timeout = 500;

                if ($("#distracted")[0].checked) {
                    timeout = 40000;
                    indemand.ui.logEvent('Invited to conference (distracted): ' + indemand.invitedConferenceId);
                } else {
                    indemand.ui.logEvent('Invited to conference (auto join): ' + indemand.invitedConferenceId);
                }

                indemand.pendingJoin = true;

                indemand.inviteTimer = setTimeout(
                    function () {
                        indemand.controllers.joinConference(indemand.invitedConferenceId);
                    },
                    timeout
                );                
            } else {
                indemand.ui.logEvent('Invited to conference (manual join): ' + indemand.invitedConferenceId);
            }
        } else if (indemand.inConference || indemand.pendingJoin) {
            if (indemand.pendingJoin) {
                indemand.ui.logEvent('Decline incoming invitation (pending join): ' + indemand.invitedConferenceId);
            } else {
                indemand.ui.logEvent('Decline incoming invitation (in conference): ' + indemand.invitedConferenceId);
            }

            indemand.ajax.declineConference(event.data, indemand.controllers.refreshAll);
        } 
    };
}

function heartbeatRefresh() {
    $("#heartbeatLabel").html(new Date().toISOString());

    if (indemand.participantId) {
        indemand.webSocket.send(indemand.participantId);
        //console.log('heartbeat');
    }

    indemand.controllers.refreshAll();
}

$("#statusLabel").html('READY');
connectWebSocket();
heartbeatRefresh();
setInterval(heartbeatRefresh, 1000);

