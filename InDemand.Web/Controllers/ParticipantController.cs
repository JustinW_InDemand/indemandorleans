using InDemand.GrainInterfaces;
using Microsoft.AspNetCore.Mvc;
using Orleans;
using System;
using System.Threading.Tasks;

namespace InDemand.Web.Controllers
{
    public class ParticipantController : Controller
    {
        [HttpPost]
        public async Task<ActionResult> Login(string participantId)
        {
            var participantGrain = GrainClient.GrainFactory.GetGrain<IParticipantGrain>(participantId);
            var success = await participantGrain.LoginParticipant("password");
            return Json(new { loginSuccess = success });
        }

        public async Task<ActionResult> Logout(string participantId)
        {
            var participantGrain = GrainClient.GrainFactory.GetGrain<IParticipantGrain>(participantId);
            await participantGrain.SetParticipantState(ParticipantState.Offline, Guid.Empty);
            return Json(new { });
        }

        public async Task<ActionResult> Break(string participantId)
        {
            var participantGrain = GrainClient.GrainFactory.GetGrain<IParticipantGrain>(participantId);
            await participantGrain.SetParticipantState(ParticipantState.OnBreak, Guid.Empty);
            return Json(new { });
        }

        public async Task<ActionResult> Resume(string participantId)
        {
            var participantGrain = GrainClient.GrainFactory.GetGrain<IParticipantGrain>(participantId);
            await participantGrain.SetParticipantState(ParticipantState.Online, Guid.Empty);
            return Json(new { });
        }
    }
}
