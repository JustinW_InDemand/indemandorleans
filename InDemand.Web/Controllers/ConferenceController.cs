using InDemand.GrainInterfaces;
using Microsoft.AspNetCore.Mvc;
using Orleans;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace InDemand.Web.Controllers
{
    public class ConferenceController : Controller
    {
        [HttpPost]
        public async Task<ActionResult> Create(string participantId)
        {
            var formDataEnm = Request.Form.GetEnumerator();
            CreateConferenceRequest conferenceCreateData = null;

            if (formDataEnm.MoveNext())
            {
                string jsonData = formDataEnm.Current.Value;

                if (jsonData != null)
                {
                    conferenceCreateData = JsonConvert.DeserializeObject<CreateConferenceRequest>(jsonData);
                }
            }

            var participantGrain = GrainClient.GrainFactory.GetGrain<IParticipantGrain>(participantId);
            var conferenceId = await participantGrain.CreateConferenceForParticipant(conferenceCreateData);
            return Json(new { conferenceId = conferenceId });
        }

        [HttpPost]
        public async Task<ActionResult> Join(string participantId, Guid conferenceId)
        {
            var participantGrain = GrainClient.GrainFactory.GetGrain<IParticipantGrain>(participantId);
            var conferenceState = await participantGrain.JoinParticipantToConference(conferenceId);
            return Json(new { conferenceState = conferenceState });
        }

        public async Task<ActionResult> Decline(string participantId, Guid conferenceId)
        {
            var conferenceGrain = GrainClient.GrainFactory.GetGrain<IConferenceGrain>(conferenceId);
            var conferenceState = await conferenceGrain.DeclineParticipantFromConference(participantId);
            return Json(new { conferenceState = conferenceState });
        }

        public async Task<ActionResult> Leave(string participantId)
        {
            var participantGrain = GrainClient.GrainFactory.GetGrain<IParticipantGrain>(participantId);
            var conferenceState = await participantGrain.LeaveParticipantFromConference();
            return Json(new { conferenceState = conferenceState });
        }
    }
}
