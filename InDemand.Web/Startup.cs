﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime.Configuration;
using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace InDemand.Web
{
    public class Startup
    {
        public Sockets socketsManager = new Sockets();

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            GrainClient.Initialize(ClientConfiguration.LocalhostSilo());
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "Domain",
                        template: "Domain/{action}",
                        defaults: new { controller = "Domain" }
                    );

                    routes.MapRoute(
                        name: "Conference",
                        template: "Conference/{action}/{participantId}/{conferenceId?}",
                        defaults: new { controller = "Conference" }
                    );

                    routes.MapRoute(
                        name: "Participant",
                        template: "Participant/{action}/{participantId}",
                        defaults: new { controller = "Participant" }
                    );

                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}");
                }
            );

            var configureTask = socketsManager.Configure(app);
            configureTask.Wait();
        }
    }
}
