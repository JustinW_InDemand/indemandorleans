﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Net.WebSockets;
using System.Threading;
using System.Text;
using InDemand.GrainInterfaces;
using Orleans;
using System.Linq;

namespace InDemand.Web
{
    public class Sockets : ICallback
    {
        private Dictionary<string, WebSocket> participantSocketLookup = new Dictionary<string, WebSocket>();

        public async void InviteParticipantToConference(string participantId, Guid conferenceId)
        {
            if (participantSocketLookup.ContainsKey(participantId))
            {
                var webSocket = participantSocketLookup[participantId];

                if (webSocket.State == WebSocketState.Open)
                {
                    var conferenceIdStr = conferenceId.ToString();
                    var buffer = ASCIIEncoding.UTF8.GetBytes(conferenceIdStr);
                    await webSocket.SendAsync(new ArraySegment<byte>(buffer, 0, conferenceIdStr.Length), WebSocketMessageType.Text, true, CancellationToken.None);
                }                
            }
        }

        internal async Task Configure(IApplicationBuilder app)
        {
            var callbackGrain = GrainClient.GrainFactory.GetGrain<ICallbackGrain>(0);
            var observerRef = await GrainClient.GrainFactory.CreateObjectReference<ICallback>(this);
            await callbackGrain.SubscribeObserver(observerRef);

            var webSocketOptions = new WebSocketOptions()
            {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
                ReceiveBufferSize = 256
            };

            app.UseWebSockets(webSocketOptions);

            app.Use(
                async (context, next) =>
                {
                    if (context.Request.Path == "/ws")
                    {
                        if (context.WebSockets.IsWebSocketRequest)
                        {
                            WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
                            await RegisterHeartbeat(context, webSocket);
                        }
                        else
                        {
                            context.Response.StatusCode = 400;
                        }
                    }
                    else
                    {
                        await next();
                    }
                }
            );
        }

        private async Task RegisterHeartbeat(HttpContext context, WebSocket webSocket)
        {
            var buffer = new byte[256];
            WebSocketReceiveResult result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

            while (!result.CloseStatus.HasValue)
            {
                var participantId = Encoding.ASCII.GetString(buffer, 0, result.Count);

                if (!participantSocketLookup.ContainsKey(participantId))
                {
                    participantSocketLookup.Add(participantId, webSocket);
                }
                else
                {
                    participantSocketLookup[participantId] = webSocket;
                }

                result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            }

            await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
        }
    }
}
