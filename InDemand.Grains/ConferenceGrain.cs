using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Orleans;
using Orleans.Concurrency;
using InDemand.GrainInterfaces;

namespace InDemand.Grains
{
    [Reentrant]
    public class ConferenceGrain : Grain, IConferenceGrain
    {
        private Guid conferenceId = Guid.Empty;
        private ConferenceState conferenceState = ConferenceState.Unknown;
        private List<ConferenceParticipantSummary> conferenceParticipantSummaries;
        private IDisposable invitationTimer = null;
        private InvitationTimerState invitationTimerState = null;

        public override Task OnActivateAsync()
        {
            conferenceState = ConferenceState.AwaitingParticipants;
            conferenceId = this.GetPrimaryKey();
            conferenceParticipantSummaries = new List<ConferenceParticipantSummary>();
            return base.OnActivateAsync();
        }
        
        public async Task<ConferenceState> JoinParticipantToConference(string participantId)
        {
            var conferenceParticipantSummary = conferenceParticipantSummaries.Find(participantId);

            if ((conferenceState & ConferenceState.Finished) != ConferenceState.Finished && (conferenceParticipantSummary == null || 
                conferenceParticipantSummary.ConferenceParticipantState == ConferenceParticipantState.Invited))
            {
                if (conferenceParticipantSummaries.Count == 0)
                {
                    var shard = ConferenceMgrGrain.GetShardFromKey(this.GetPrimaryKey());
                    var conferenceMgrGrain = GrainFactory.GetGrain<IConferenceMgrGrain>(shard);
                    await conferenceMgrGrain.AddConference(this.GetPrimaryKey());
                }

                if (conferenceParticipantSummary == null)
                {
                    conferenceParticipantSummary = new ConferenceParticipantSummary();
                    conferenceParticipantSummaries.Add(conferenceParticipantSummary);
                }

                if (invitationTimer != null && conferenceParticipantSummary.ConferenceParticipantState == ConferenceParticipantState.Invited)
                {
                    invitationTimer.Dispose();
                    invitationTimer = null;
                }

                conferenceParticipantSummary.ParticipantId = participantId;
                conferenceParticipantSummary.ConferenceParticipantState = ConferenceParticipantState.Joined;

                if ((conferenceState & ConferenceState.AwaitingParticipants) == ConferenceState.AwaitingParticipants &&
                    conferenceParticipantSummaries.FindByState(ConferenceParticipantState.Joined).Count() > 1)
                {
                    conferenceState = ConferenceState.InProgress;
                }

                var participantGrain = GrainFactory.GetGrain<IParticipantGrain>(participantId);
                await participantGrain.SetParticipantState(ParticipantState.InConference, this.GetPrimaryKey());
            }

            return conferenceState;
        }

        private async Task DisinviteParticipantFromConference(string participantId)
        {
            var conferenceParticipantSummary = conferenceParticipantSummaries.Find(participantId);

            if ((conferenceState & ConferenceState.Finished) != ConferenceState.Finished && conferenceParticipantSummary.ConferenceParticipantState == ConferenceParticipantState.Invited)
            {
                conferenceParticipantSummary.ConferenceParticipantState = ConferenceParticipantState.Disinvited;
                var participantGrain = GrainFactory.GetGrain<IParticipantGrain>(participantId);
                await participantGrain.SetParticipantState(ParticipantState.Online, Guid.Empty);
            }
        }

        public async Task InviteParticipantToConference(string participantId)
        {
            var conferenceParticipantSummary = conferenceParticipantSummaries.Find(participantId);

            if ((conferenceState & ConferenceState.Finished) != ConferenceState.Finished && (conferenceParticipantSummary == null ||
                conferenceParticipantSummary.ConferenceParticipantState != ConferenceParticipantState.Joined))
            {
                var participantGrain = GrainFactory.GetGrain<IParticipantGrain>(participantId);

                if (await participantGrain.InviteParticipantToConference(this.GetPrimaryKey()))
                {
                    if (conferenceParticipantSummary == null)
                    {
                        conferenceParticipantSummary = new ConferenceParticipantSummary();
                        conferenceParticipantSummaries.Add(conferenceParticipantSummary);
                    }

                    conferenceParticipantSummary.ParticipantId = participantId;
                    conferenceParticipantSummary.ConferenceParticipantState = ConferenceParticipantState.Invited;
                    await participantGrain.SetParticipantState(ParticipantState.Invited, Guid.Empty);
                }
            }
        }

        public async Task<ConferenceState> DeclineParticipantFromConference(string participantId)
        {
            var conferenceParticipantSummary = conferenceParticipantSummaries.Find(participantId);

            if (conferenceParticipantSummary != null)
            {
                conferenceParticipantSummary.ConferenceParticipantState = ConferenceParticipantState.Declined;
                var participantGrain = GrainFactory.GetGrain<IParticipantGrain>(participantId);
                await participantGrain.SetParticipantState(ParticipantState.Online, Guid.Empty);
                await InvitationTimerCallback(invitationTimerState);
            }

            return conferenceState;
        }

        public async Task<ConferenceState> LeaveParticipantFromConference(string participantId)
        {
            var conferenceParticipantSummary = conferenceParticipantSummaries.Find(participantId);

            if (conferenceParticipantSummary != null)
            {
                conferenceParticipantSummary.ConferenceParticipantState = ConferenceParticipantState.Left;
                var joinedCount = conferenceParticipantSummaries.FindByState(ConferenceParticipantState.Joined).Count();

                if (joinedCount == 0)
                {
                    conferenceState = ConferenceState.Finished;
                    var shard = ConferenceMgrGrain.GetShardFromKey(this.GetPrimaryKey());
                    var conferenceMgrGrain = GrainFactory.GetGrain<IConferenceMgrGrain>(shard);
                    await conferenceMgrGrain.RemoveConference(this.GetPrimaryKey());
                }
                else if (joinedCount == 1)
                {
                    conferenceState = ConferenceState.AwaitingParticipants;
                }

                var participantGrain = GrainFactory.GetGrain<IParticipantGrain>(participantId);
                await participantGrain.SetParticipantState(ParticipantState.Online, Guid.Empty);
            }

            return conferenceState;
        }

        public Task<ConferenceState> GetConferenceState()
        {
            return Task.FromResult(conferenceState);
        }

        public Task<ConferenceSummary> GetConferenceSummary()
        {
            return Task.FromResult(
                new ConferenceSummary
                {
                    ConferenceId = this.GetPrimaryKey(),
                    ConferenceState = conferenceState,
                    ConferenceParticipantSummaries = conferenceParticipantSummaries.ToArray()
                }
            );
        }

        private class InvitationTimerState
        {
            public List<ParticipantSummary> allParticipantSummaries = new List<ParticipantSummary>();
            public int currentIndex = -1;
        }

        private async Task InvitationTimerCallback(object state)
        {
            var invitationTimerState = (InvitationTimerState)state;

            if (invitationTimer != null)
            {
                invitationTimer.Dispose();
                invitationTimer = null;
            }

            if (invitationTimerState != null)
            {
                var numParticipants = invitationTimerState.allParticipantSummaries.Count;

                if ((conferenceState & ConferenceState.AwaitingParticipants) == ConferenceState.AwaitingParticipants)
                {
                    if (invitationTimerState.currentIndex >= 0)
                    {
                        var participantSummary = invitationTimerState.allParticipantSummaries[invitationTimerState.currentIndex];
                        await DisinviteParticipantFromConference(participantSummary.ParticipantId);
                    }

                    if (invitationTimerState.currentIndex == numParticipants - 1)
                    {
                        conferenceState = ConferenceState.Finished | ConferenceState.BusySignal;
                    }
                    else
                    {
                        for (var i = invitationTimerState.currentIndex + 1; i < numParticipants; i++)
                        {
                            var participantSummary = invitationTimerState.allParticipantSummaries[i];
                            invitationTimerState.currentIndex++;

                            if (participantSummary.ParticipantState == ParticipantState.Online)
                            {
                                var conferenceParticipantSummary = conferenceParticipantSummaries.Find(participantSummary.ParticipantId);

                                if (conferenceParticipantSummary == null)
                                {
                                    await InviteParticipantToConference(participantSummary.ParticipantId);
                                    break;
                                }
                            }
                        }

                        invitationTimer = RegisterTimer(InvitationTimerCallback, invitationTimerState, TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(30));
                    }
                }
            }
        }

        public async Task RouteConference(CreateConferenceRequest createConferenceRequest)
        {
            invitationTimerState = new InvitationTimerState();

            for (int i = 0; i < 256; i++)
            {
                var participantMgrGrain = GrainFactory.GetGrain<IParticipantMgrGrain>(i);
                var participantSummaries = await participantMgrGrain.GetParticipantSummaries();
                invitationTimerState.allParticipantSummaries.AddRange(participantSummaries);
            }

            await InvitationTimerCallback(invitationTimerState);           
        }
    }
}
