using System;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using Orleans;
using Orleans.Concurrency;
using InDemand.GrainInterfaces;
using System.Collections.Generic;

namespace InDemand.Grains
{
    [Reentrant]
    public class ConferenceMgrGrain : Grain, IConferenceMgrGrain
    {
        private MemoryCache conferences;

        public override Task OnActivateAsync()
        {
            conferences = new MemoryCache("conferences");
            return base.OnActivateAsync();
        }

        public static int GetShardFromKey(Guid conferenceId)
        {
            return conferenceId.ToByteArray()[0];
        }

        public Task AddConference(Guid conferenceId)
        {
            conferences.Add(conferenceId.ToString(), true, new DateTimeOffset(DateTime.UtcNow).AddHours(1));
            return TaskDone.Done;
        }

        public Task RemoveConference(Guid conferenceId)
        {
            conferences.Remove(conferenceId.ToString());
            return TaskDone.Done;
        }

        public async Task<ConferenceSummary[]> GetConferenceSummaries()
        {
            var conferenceSummaryPromises = new List<Task<ConferenceSummary>>();

            foreach (var kvp in conferences)
            {
                conferenceSummaryPromises.Add(GrainFactory.GetGrain<IConferenceGrain>(Guid.Parse(kvp.Key)).GetConferenceSummary());
            }

            await Task.WhenAll(conferenceSummaryPromises);
            return conferenceSummaryPromises.Select(x => x.Result).ToArray();
        }
    }
}
