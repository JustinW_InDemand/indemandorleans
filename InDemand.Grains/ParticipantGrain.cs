using Orleans;
using InDemand.GrainInterfaces;
using System;
using System.Threading.Tasks;
using Orleans.Concurrency;

namespace InDemand.Grains
{
    [Reentrant]
    public class ParticipantGrain : Grain, IParticipantGrain
    {
        private ParticipantState participantState = ParticipantState.Unknown;
        private Guid currentConferenceId = Guid.Empty;

        public async Task<Guid> CreateConferenceForParticipant(CreateConferenceRequest createConferenceRequest)
        {
            var conferenceId = Guid.NewGuid();
            var conferenceGrain = GrainFactory.GetGrain<IConferenceGrain>(conferenceId);
            var conferenceState = await conferenceGrain.JoinParticipantToConference(this.GetPrimaryKeyString());

            if ((conferenceState & ConferenceState.AwaitingParticipants) == ConferenceState.AwaitingParticipants)
            {
                if (createConferenceRequest != null)
                {           
                    await conferenceGrain.RouteConference(createConferenceRequest);
                }

                return conferenceId;
            }

            return Guid.Empty;
        }

        public async Task<ConferenceState> JoinParticipantToConference(Guid conferenceId)
        {
            if (currentConferenceId == Guid.Empty && participantState == ParticipantState.Invited)
            {
                var conferenceGrain = GrainFactory.GetGrain<IConferenceGrain>(conferenceId);
                return await conferenceGrain.JoinParticipantToConference(this.GetPrimaryKeyString());
            }
            else
            {
                return (byte)ConferenceState.Unknown;
            }
        }

        public async Task<bool> InviteParticipantToConference(Guid conferenceId)
        {
            if (currentConferenceId == Guid.Empty && participantState == ParticipantState.Online)
            {
                var callbackGrain = GrainFactory.GetGrain<ICallbackGrain>(0);
                return await callbackGrain.CallbackInviteParticipantToConference(this.GetPrimaryKeyString(), conferenceId);
            }

            return false;
        }        

        public async Task<ConferenceState> LeaveParticipantFromConference()
        {
            if (currentConferenceId != Guid.Empty)
            {
                var conferenceGrain = GrainFactory.GetGrain<IConferenceGrain>(currentConferenceId);
                return await conferenceGrain.LeaveParticipantFromConference(this.GetPrimaryKeyString());
            }
            else
            {
                return (byte)ConferenceState.Unknown;
            }
        }

        public async Task<bool> LoginParticipant(string password)
        {
            if (participantState == ParticipantState.Offline || participantState == ParticipantState.Unknown)
            {
                currentConferenceId = Guid.Empty;
                participantState = ParticipantState.Online;
                var shard = ParticipantMgrGrain.GetShardFromKey(this.GetPrimaryKeyString());
                var participantMgrGrain = GrainFactory.GetGrain<IParticipantMgrGrain>(shard);
                await participantMgrGrain.AddParticipant(this.GetPrimaryKeyString());
            }

            return true;
        }

        public Task ResetParticipantState(bool hard)
        {
            if (hard)
            {
                participantState = ParticipantState.Unknown;
            }
            else
            {
                if (participantState == ParticipantState.InConference ||
                    participantState == ParticipantState.Invited)
                {
                    participantState = ParticipantState.Online;
                }
            }

            currentConferenceId = Guid.Empty;
            return TaskDone.Done;
        }

        public Task<bool> SetParticipantState(ParticipantState participantState, Guid conferenceId)
        {
            bool canTransition = false;

            ParticipantState[][] transitions =
                {                    
                    new ParticipantState[] /*ParticipantState.Offline*/      { ParticipantState.Online, ParticipantState.OnBreak },
                    new ParticipantState[] /*ParticipantState.Online*/       { ParticipantState.Offline, ParticipantState.OnBreak, ParticipantState.Invited, ParticipantState.InConference },
                    new ParticipantState[] /*ParticipantState.OnBreak*/      { ParticipantState.Offline, ParticipantState.Online },
                    new ParticipantState[] /*ParticipantState.Invited*/      { ParticipantState.Online, ParticipantState.InConference},
                    new ParticipantState[] /*ParticipantState.InConference*/ { ParticipantState.Online }
                };

            // we can always transition from or to the unknown state
            if (this.participantState == ParticipantState.Unknown || participantState == ParticipantState.Unknown)
            {
                canTransition = true;
            }
            else
            {
                var transitionRow = transitions[(int)this.participantState - 1];

                for (int i = 0; i < transitionRow.Length; i++)
                {
                    if (transitionRow[i] == participantState)
                    {
                        canTransition = true;
                        break;
                    }
                }
            }

            if (canTransition)
            {
                if (participantState == ParticipantState.InConference)
                {
                    currentConferenceId = conferenceId;
                }
                else if (this.participantState == ParticipantState.InConference && participantState == ParticipantState.Online)
                {
                    currentConferenceId = Guid.Empty;
                }

                this.participantState = participantState;
            }

            return Task.FromResult(canTransition);
        }

        public Task<ParticipantState> GetParticipantState()
        {
            return Task.FromResult(participantState);
        }

        public Task<ParticipantSummary> GetParticipantSummary()
        {
            return Task.FromResult(
                new ParticipantSummary
                {
                    ParticipantId = this.GetPrimaryKeyString(),
                    ParticipantState = participantState,
                    ConferenceId = currentConferenceId
                }
            );
        }
    }
}
