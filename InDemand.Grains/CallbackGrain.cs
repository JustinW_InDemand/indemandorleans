﻿using InDemand.GrainInterfaces;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InDemand.Grains
{
    public class CallbackGrain : Grain, ICallbackGrain
    {
        private ObserverSubscriptionManager<ICallback> subscribers = new ObserverSubscriptionManager<ICallback>();

        public Task SubscribeObserver(ICallback observer)
        {
            subscribers.Subscribe(observer);
            return TaskDone.Done;
        }

        public Task<bool> CallbackInviteParticipantToConference(string participantId, Guid conferenceId)
        {
            try
            {
                subscribers.Notify(x => x.InviteParticipantToConference(participantId, conferenceId));
            }
            catch
            {
                return Task.FromResult(false);   
            }

            return Task.FromResult(true);
        }
    }
}
